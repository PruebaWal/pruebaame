

protocol CustomAlertViewDelegate: class {
    func okButtonTapped()
    func cancelButtonTapped()
}

protocol CustomAlertViewMessageDelegate: class {
    func okButtonTappedMessage()
    func cancelButtonTappedMessage()
}
