

import Foundation
import UIKit


protocol ServicesDelegate {
    func responseService(type:ServicesTypes,endpoint:String, response:NSDictionary)
}


//solo cuenta con servicios get actualmente
enum ServicesTypes {
    case GET
    case PUT
    case POST
    case DELETE
    case PATCH
}

//objeto que almacena la informacion del servicio
class Services: NSObject{
    
    //delegado
    var delegate: ServicesDelegate?
    
    //cuerpo del objeto
    var type : ServicesTypes?
    var endpoint : String?
    
    //url principal del servidor
    var urlServices :String! = "https://api.themoviedb.org/3/movie/"
   

    
    
    
  
    
    //metodo que ejecuta la consulta 
    func send(type: ServicesTypes,url: String,params:NSDictionary,message:String){
     
        
        self.endpoint = url
        self.type = type
        
        var config:LoaderView.Config = LoaderView.Config()
        config.size = 100;
        config.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8);
        config.spinnerColor    = UIColor.white;
        config.titleTextColor  = UIColor.white;
        LoaderView.setConfig(config: config);
        
        LoaderView.show(title: message, animated: true)
        
        
        let stringUrl = "\(urlServices!)\(url)"
  
        let url = URL(string: stringUrl)
        
        let task = URLSession.shared.dataTask(with: url!) { data, response, error in
            guard error == nil else {
                print(error!)
               DispatchQueue.main.async {LoaderView.hide()}
                return
            }
            guard let data = data else {
                print("Data is empty")
                DispatchQueue.main.async {LoaderView.hide()}
                return
            }
            
            let json = try! JSONSerialization.jsonObject(with: data, options: [])
            print(json)
            let dataJson = json as! NSDictionary
            
            DispatchQueue.main.async {LoaderView.hide()}
           self.delegate?.responseService(type: self.type!, endpoint: self.endpoint!, response: dataJson)
            
            
            
        }
       
        
        task.resume()
        
    }
    
 
    
}





