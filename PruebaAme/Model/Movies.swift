

import Foundation
import UIKit
import CoreData




//objeto que almacena la informacion de una pelicula
class Movies: NSObject{
    
    
    
    //cuerpo del objeto
    var titulo : String!
    var movieDescription : String!
    var rating : String!
    var imagenURL : String!
    var release_date : String!
    var imageData : NSData?
    //se asigna igualmente una imagen defecto para que aparezca mientras carga
    var imagen : UIImage! = #imageLiteral(resourceName: "profileDefect")

   

    
    
    
    
  //constructor nuevo elemento
    init(titulo : String ,movieDescription : String,rating:String,imagenURL:String,release_date:String) {
        
        super.init()
        self.titulo = titulo
        self.movieDescription = movieDescription
        self.rating = rating
        self.imagenURL = imagenURL
        self.release_date = release_date
      
        //descarga de imagen
        self.getImageAndSave(imagenURL: imagenURL)
        
}
    
    
    //constructor ya en coreData
    init(titulo : String ,movieDescription : String,rating:String,imagenData:NSData,release_date:String) {
        
        super.init()
        self.titulo = titulo
        self.movieDescription = movieDescription
        self.rating = rating
        self.imageData = imagenData
        self.release_date = release_date
 
    }
    
    //metodo que descarga la imagen y la guardar el objeto en coreData
    func getImageAndSave(imagenURL: String){
        
        
        let url = URL(string: imagenURL)
        
        if url != nil{
        print("inicia descarga")
        getData(from: url!) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url!.lastPathComponent)
            print("termina descarga")
            DispatchQueue.main.async() {
            
                if let imagen = UIImage(data: data){
                    
                    self.imagen = imagen
                    
                    //se notifica al controlador para que muestre la imagen
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "imageUpdate"), object: nil)
                    
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    let context = appDelegate.persistentContainer.viewContext
                    let entity = NSEntityDescription.entity(forEntityName: "Movie", in: context)
                    let newMovie = NSManagedObject(entity: entity!, insertInto: context)
                    let imageData = self.imagen.pngData() as NSData?
                     newMovie.setValue(imageData, forKey: "imagenUrl")
                    newMovie.setValue(self.movieDescription!, forKey: "movieDescription")
                    newMovie.setValue(self.rating!, forKey: "rating")
                    newMovie.setValue(self.titulo!, forKey: "titulo")
                    newMovie.setValue(self.release_date!, forKey: "releaseDate")
                    do{
                    try context.save()
                    
                    }catch{
                        print("error")
                        
                    }
                    
                }
           
            }
        }
        }else{
            
            //aqui podriamos gestionar si hay algun error con la imagen tomar otra accion como poner una defecto etc.
            
        }
    }
    
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    
  
    
}





