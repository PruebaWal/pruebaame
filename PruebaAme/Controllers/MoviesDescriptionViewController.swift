
import UIKit




class MoviesDescriptionViewController: UIViewController {
    
    //objeto que se recibe de la vista pasada
    var movie : Movies!
    
    
    //referencias de los componentes visuales
    @IBOutlet weak var nameMovieLabel: UILabel!
    
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var releaseTimeLabel: UILabel!
    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var ratingLabel: UILabel!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
     
        //ponemos la informacion del objeto recibido
  
            self.movieImage.image = self.movie.imagen
            self.nameMovieLabel.text = self.movie.titulo!
            self.ratingLabel.text = self.movie.rating!
            self.descriptionTextView.text = self.movie.movieDescription!
            self.releaseTimeLabel.text = self.movie.release_date!
  
    }
    
   
    
    
    @IBAction func notificationsButtonAction(_ sender: UIButton) {
        
        alert(title: "¡Oops!", message: "Función no disponible actualmente.", cancel: "Cerrar");
        
    }
    
    
    //metodo que regresa a la vista de las noticias
    @IBAction func returnAction(_ sender: UIButton) {
        
        _ = self.navigationController?.popViewController(animated: true)
        
    }
    
    
  
    
    
    
    
    
}


