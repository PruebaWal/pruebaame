

import UIKit
import CoreData



class MoviesViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource,ServicesDelegate{
  
    //referencia de la tableView de las noticias
    @IBOutlet weak var tableView: UITableView!
    
    //arreglo que controla la tableView
    var movies : [Movies] = []
    
    
    //objeto que controlara los servicios
    let servicesNatives = Services()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        
      //esperamos cambios en el modelo
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadItemsInTableView), name: NSNotification.Name(rawValue: "imageUpdate"), object: nil)
        
        
        //metodo que revisa si es tiempo (24 horas)para llamar al servicio
        self.checkTimeService()
        
      
        
        //delegado servicios
         self.servicesNatives.delegate = self
        
       
     

        
        self.tableView.separatorStyle = .none
        
        //registro de tabla
        self.tableView.register(UINib(nibName: "MoviesTableViewCell", bundle: nil), forCellReuseIdentifier: "MoviesTableViewCell")
     
        
        
        

       
        
    }
    
   
    
    //metodo del delegado de tableview
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let vc = MoviesDescriptionViewController()
        
        
        //mandamos el objeto seleccionado
       vc.movie = self.movies[indexPath.row]
        
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    
    
    
    //metodos del datasource del tableview
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return  self.movies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       
        
        
        
        let cell: MoviesTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "MoviesTableViewCell") as! MoviesTableViewCell
        
        
        cell.movieTextLabel.text = self.movies[indexPath.row].titulo!
        cell.dateTextLabel.text = self.movies[indexPath.row].release_date!
        cell.selectionStyle = .none
        
        if self.movies[indexPath.row].imageData != nil{
            
            let image = UIImage(data: self.movies[indexPath.row].imageData! as Data)
            self.movies[indexPath.row].imagen = image
            cell.movieImageView.image = self.movies[indexPath.row].imagen
        }else{
        cell.movieImageView.image = self.movies[indexPath.row].imagen
        }
        
        
        return cell
        
        
    }
  
    
    //metodo que se ejecuta al presionar el boton del menu
    @IBAction func menuShow(_ sender: UIButton) {
                  alert(title: "¡Oops!", message: "Función no disponible actualmente.", cancel: "Cerrar");
        
    }
    
    //metodo que revisa si es tiempo para llamar al servicio
    func checkTimeService(){
        
        
        if DataUser.DATE != nil{
            
            // revisamos la diferencia de tiempo
            
            let date = Date()
            let newDateFormat = DateFormatter()
            newDateFormat.dateStyle = .medium
            newDateFormat.timeStyle = .none
            newDateFormat.dateFormat = "dd/MM/yyyy HH:mm"
            
    print("esta es la guardada \(DataUser.DATE!)")
            let dateLast = newDateFormat.date(from: DataUser.DATE!)
           
            //obtenemos la diferencia en horas gracias a una extension que se le dio a Date
            let hoursDiference = date.hours(from: dateLast!)
            print("esta es la diferencia \(hoursDiference)")
            
            if hoursDiference >= 24{
                self.deleteAllElementsCoreData()
                self.servicesNatives.send(type: .GET, url: "top_rated?api_key=60a3b9705ed735a49fd2a5fd7cae05ab&language=es&page=1", params: [:],message:"Obteniendo")
   
            }else{
                //utilizamos core data porque aun no pasan 24 horas
                self.loadElementsCoreData()
                
            }
            
            
            
        }else{
            //es la primera ves que se ejecuta la app llamamos al servicio
            self.servicesNatives.send(type: .GET, url: "top_rated?api_key=60a3b9705ed735a49fd2a5fd7cae05ab&language=es&page=1", params: [:],message:"Obteniendo")

        }
        
        
    }

    
    //metodo que elimina los objetos que teniamos guardados
    func deleteAllElementsCoreData(){
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Movie")
        
        request.returnsObjectsAsFaults = false
        do {
        
          let result = try context.fetch(request)
            for object in result as! [NSManagedObject] {
                context.delete(object)
            }
             try context.save()
        
        }catch{
            print("error al borrar elemento core data")
        }
        
    }
   
    //metodo que carga los elementos de coreData
    func loadElementsCoreData(){
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Movie")
        
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
            
                let titulo = data.value(forKey: "titulo") as! String
                let rating = data.value(forKey: "rating") as! String
                let movieDescription = data.value(forKey: "movieDescription") as! String
                let release_date = data.value(forKey: "releaseDate") as! String
                let imagenData = data.value(forKey: "imagenUrl") as! NSData
                
                let objectTemp = Movies(titulo: titulo, movieDescription: movieDescription, rating: rating, imagenData: imagenData,release_date: release_date)
                self.movies.append(objectTemp)
               
                
            }
            
              self.movies.sort(by: { $0.rating.compare($1.rating) == .orderedDescending })
            DispatchQueue.main.async {
              
                self.tableView.reloadData()
            }
            
        } catch {
            
            print("Failed")
        }
        
        
    }
   
   
    @IBAction func notificationsButtonAction(_ sender: UIButton) {
      alert(title: "¡Oops!", message: "Función no disponible actualmente.", cancel: "Cerrar");
        
    }
    

    //metodo llamado por la notificacion al descargar una imagen 
   @objc func reloadItemsInTableView(){
        
        self.tableView.reloadData()
    }
    
    
    
    //metodo que regresa la respuesta del servicio
    func responseService(type: ServicesTypes, endpoint: String, response: NSDictionary) {

        let allMovies = response["results"] as! NSArray
        
        var counter = 0
        self.movies = []
        for movie in allMovies{
   
            let dictio = movie as! NSDictionary
            let title = dictio["title"] as! String
            let movieDescription = dictio["overview"] as! String
            let rating = dictio["vote_average"] as! Double
            let release_date = dictio["release_date"] as! String
            let imageURL = "https://image.tmdb.org/t/p/w600_and_h900_bestv2/\(dictio["poster_path"] as! String)"
            
            let objectTemp = Movies(titulo: title, movieDescription: movieDescription, rating: String(rating), imagenURL: imageURL,release_date: release_date)
            self.movies.append(objectTemp)
            
            counter = counter + 1
            if counter >= 10{
                break
            }
            
        }
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
            
            
            //asignamos el tiempo inicial para esperar 24 horas
            let date = Date()
            let newDateFormat = DateFormatter()
            newDateFormat.dateStyle = .medium
            newDateFormat.timeStyle = .none
            newDateFormat.dateFormat = "dd/MM/yyyy HH:mm"
            
            let dateString = newDateFormat.string(from: date)
            DataUser.DATE = dateString
            
        }
        
    }
    
   

   
}




//extension necesaria para poder controlar la diferencia de horas de la ultima consulta
extension Date {
    
    /// diferencia de horas en dos dates
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    
    /// regresa informacion del intervalo de dos dates
    func offset(from date: Date) -> String {
        
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        
        return ""
    }
}
