

import UIKit

class MoviesTableViewCell: UITableViewCell {

   
   
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var movieTextLabel: UILabel!
    @IBOutlet weak var dateTextLabel: UILabel!
    
}
